This is a test of Build and programm environment for STM32 Discovery board.

To build code type: make all
As a result you should get file build/stm32f4_sample.elf

To programm the board:
openocd -f interface/stlink-v2.cfg -c "set WORKAREASIZE 0x2000" -f target/stm32f4x_stlink.cfg -c "program build/stm32f4_sample.elf  verify res
et"

To start debugging:
In one terminal start debugging server:
openocd -f stm32f4discovery.cfg

Start debug session:
arm-none-eabi-gdb

target remote localhost:3333
monitor reset
monitor halt
load
.....
<Use GDB as you always do>


Compile flags for debugging and optimization:
Optimization of the code:
CFLAGS  = -std=gnu99 -g -O2 -Wall -Tstm32_flash.ld
Debug of the code:
CFLAGS  = -std=gnu99 -g -O0 -Wall -Tstm32_flash.ld

