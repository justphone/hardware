EESchema Schematic File Version 2
LIBS:ChipResistor
LIBS:Schottky
LIBS:SchottkyPack
LIBS:PowerInductors
LIBS:BatteryChargers
LIBS:power
LIBS:LDO
LIBS:Atmel
LIBS:MicAmps
LIBS:ADCs
LIBS:MOSFETs
LIBS:SPI
LIBS:m95
LIBS:DACs
LIBS:SpeakerAmps
LIBS:mma8652fcr1
LIBS:HC49
LIBS:CeramicCapacitors
LIBS:74
LIBS:Switches
LIBS:conn
LIBS:easyvr
LIBS:ABMM
LIBS:2403_260_00031
LIBS:Mic
LIBS:TVS
LIBS:Indication
LIBS:TantalumCapacitors
LIBS:PushButtons
LIBS:sim
LIBS:Standard
LIBS:JustPhone-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1550 1500 1450 1100
U 543ABB40
F0 "Power" 60
F1 "Power.sch" 60
$EndSheet
$Sheet
S 4250 1700 1650 1250
U 543ADB40
F0 "MCU" 60
F1 "MCU.sch" 60
$EndSheet
$Sheet
S 6700 2050 1900 1400
U 544BC94B
F0 "Audio Path" 60
F1 "AudioPath.sch" 60
$EndSheet
$Sheet
S 6900 4000 2100 1500
U 54508347
F0 "GSM Module" 60
F1 "GSM.sch" 60
$EndSheet
$EndSCHEMATC
